﻿using DevFoldersToClean;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using VSFoldersToClean.Classes;

namespace VSFoldersToClean
{
    public partial class frmMain : Form
    {
        #region " Enumerators "

        public enum ContentType
        {
            File,
            Folder
        }

        #endregion

        #region " Fields "

        private List<string> _colFoldersToDelete = new List<string>();
        private List<string> _colFilesToDelete = new List<string>();
        private bool _allowRefreshFolders = true;

        #endregion

        #region " Constructor "

        public frmMain()
        {
            InitializeComponent();
            DevFolders folders = new DevFolders();
            folders.LoadFolders();

            gridFolders.DataSource = folders;
        }

        #endregion

        #region " Form Events "

        private void gridFolders_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (_allowRefreshFolders)
            {
                gridFolders.Rows[e.RowIndex].Cells["colNoFiles"].Value =
                    Directory.GetDirectories(gridFolders.Rows[e.RowIndex].Cells["colFolderName"].Value.ToString(), "*.*", SearchOption.AllDirectories).Count() + " Folders, " +
                    Directory.GetFiles(gridFolders.Rows[e.RowIndex].Cells["colFolderName"].Value.ToString(), "*.*", SearchOption.AllDirectories).Count() + " Files";
            }

            if (e.RowIndex == gridFolders.Rows.Count - 1)
                _allowRefreshFolders = false;
        }

        private void gridFolders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Process.Start("explorer.exe", gridFolders.Rows[e.RowIndex].Cells["colFolderName"].Value.ToString());
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            for (int rowIndex = 0; rowIndex < gridFolders.Rows.Count; rowIndex++)
            {
                if (Convert.ToBoolean(gridFolders.Rows[rowIndex].Cells["colSelect"].Value))
                {
                    IncludeContents(gridFolders.Rows[rowIndex].Cells["colFolderName"].Value.ToString());
                }
            }

            backgroundDelete.RunWorkerAsync();
        }

        private void backgroundDelete_DoWork(object sender, DoWorkEventArgs e)
        {
            progressDelete.PerformSafely(() => progressDelete.Value = 0);
            progressDelete.PerformSafely(() => progressDelete.Maximum = _colFilesToDelete.Count + _colFoldersToDelete.Count);
            progressDelete.PerformSafely(() => progressDelete.Visible = true);
            rtbLog.PerformSafely(() => rtbLog.ResetText());
            rtbLog.PerformSafely(() => rtbLog.AppendText("CONTENT WITH ERROR:\n\n"));

            foreach (var file in _colFilesToDelete)
                DeleteContent(file, ContentType.File);

            foreach (var folder in _colFoldersToDelete)
                DeleteContent(folder, ContentType.Folder);

            progressDelete.PerformSafely(() => progressDelete.Visible = false);
        }

        private void backgroundDelete_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _allowRefreshFolders = true;
            gridFolders.PerformSafely(() => gridFolders.Refresh());
            _colFoldersToDelete.Clear();
            _colFilesToDelete.Clear();
            MessageBox.Show("Delete process finished! You can see the log for erros.");
        }

        #endregion

        #region " Methods "

        private void IncludeContents(string path)
        {
            _colFoldersToDelete = _colFoldersToDelete.Concat(Directory.GetDirectories(path)).ToList();
            _colFilesToDelete = _colFilesToDelete.Concat(Directory.GetFiles(path, "*.*")).ToList();
        }

        private void DeleteContent(string content, ContentType contentType)
        {
            try
            {
                if (contentType == ContentType.File)
                    File.Delete(content);
                else
                    Directory.Delete(content, true);

                progressDelete.PerformSafely(() => progressDelete.Value += 1);
            }
            catch (Exception)
            {
                rtbLog.PerformSafely(() => rtbLog.AppendText(content + "\n"));
            }
        }

        #endregion
    }
}
