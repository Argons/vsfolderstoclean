﻿using System;

// Taken from http://www.codemag.com/article/0907031
namespace DevFoldersToClean
{
  class DevFolder
  {
    public DevFolder(string description)
    {
      Description = description;
    }

    public string Description { get; set; }
    public string FolderName { get; set; }
  }
}
