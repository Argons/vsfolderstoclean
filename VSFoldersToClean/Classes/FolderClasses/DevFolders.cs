﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Win32;

// Taken from http://www.codemag.com/article/0907031
namespace DevFoldersToClean
{
  class DevFolders : List<DevFolder>
  {
    public void LoadFolders()
    {
      DevFolder folder;

      folder = new DevFolder("TEMP");
      folder.FolderName = Environment.GetEnvironmentVariable("TEMP");
      this.Add(folder);

      if (folder.FolderName != Environment.GetEnvironmentVariable("TMP"))
      {
        folder = new DevFolder("TMP");
        folder.FolderName = Environment.GetEnvironmentVariable("TMP");
        this.Add(folder);
      }

      folder = new DevFolder("Internet Cache");
      folder.FolderName = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache);
      this.Add(folder);

      // Get .NET Folders
      DotNetLoad();
    }

    private void DotNetLoad()
    {
      DevFolder folder;
      DotNetVersions versions;

      versions = new DotNetVersions();
      versions.BuildCollectionOfDotNetVersions();

      foreach (DotNetVersion version in versions)
      {
        if (version.TempASPNETPath != string.Empty)
        {
          folder = new DevFolder(version.Name + " - Temporary ASP.NET Files");
          folder.FolderName = version.TempASPNETPath;
          this.Add(folder);
        }

        if (version.VSWebCacheFolderPath != string.Empty)
        {
          folder = new DevFolder(version.Name + " - WebCache");
          folder.FolderName = version.VSWebCacheFolderPath;
          this.Add(folder);
        }

        if (version.BackupFolderPath != string.Empty)
        {
          folder = new DevFolder(version.Name + " - VS.NET Backup");
          folder.FolderName = version.BackupFolderPath;
          this.Add(folder);
        }

        if (version.AssembliesFolderPath != string.Empty)
        {
          folder = new DevFolder(version.Name + " - Assembly Cache");
          folder.FolderName = version.AssembliesFolderPath;
          this.Add(folder);
        }

        if (version.ProjectAssembliesFolderPath != string.Empty)
        {
          folder = new DevFolder(version.Name + " - Project Assemblies");
          folder.FolderName = version.ProjectAssembliesFolderPath;
          this.Add(folder);
        }
      }
    }
  }
}
