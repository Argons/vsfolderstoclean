using System;
using System.IO;
using System.Collections.Generic;

// Taken from http://www.codemag.com/article/0907031
namespace DevFoldersToClean
{
  class DotNetVersions : System.Collections.Generic.List<DotNetVersion>
  {
    #region Build Collection of .NET Versions - Only 2.0 and 3.5
    public void BuildCollectionOfDotNetVersions()
    {
      DotNetVersion version = null;

      //  Create Version 2.0
      version = new DotNetVersion();
      version.VSNETName = "Visual Studio 2005";
      version.Name = ".NET 2.0";
      version.Number = "v2.0.50727";
      version.RegKey = @"Software\Microsoft\VisualStudio\8.0\";
      version.VersionNumber = "8.0";
      version.BuiltOn = "";
      version.Init();
      this.Add(version);

      //  Create Version 3.5
      version = new DotNetVersion();
      version.VSNETName = "Visual Studio 2008";
      version.Name = ".NET 3.5";
      version.Number = "V3.5";
      version.RegKey = @"Software\Microsoft\VisualStudio\9.0\";
      version.BuiltOn = ".NET 2.0";
      version.VersionNumber = "9.0";
      version.Init();
      this.Add(version);

      //  Create Version 4
      version = new DotNetVersion();
      version.VSNETName = "Visual Studio 2010";
      version.Name = ".NET 4.0";
      version.Number = "V4.0.30319";
      version.RegKey = @"Software\Microsoft\VisualStudio\10.0\";
      version.VersionNumber = "10.0";
      version.Init();
      this.Add(version);

      CheckFolders();
    }
    #endregion

    #region Check Folders
    private void CheckFolders()
    {
      //  Check for valid Folders
      foreach (DotNetVersion version in this)
      {
        if (Directory.Exists(version.TempASPNETPath) == false)
        {
          version.TempASPNETPath = string.Empty;
          if (version.BuiltOn != string.Empty)
          {
            DotNetVersion dv = null;

            dv = this.GetByVSNetName(version.BuiltOn);
            if (dv != null)
            {
              //  Get Temp ASP.NET Path from this version
              version.TempASPNETPath = dv.TempASPNETPath;
            }
          }
        }
        if (Directory.Exists(version.AssembliesFolderPath) == false)
        {
          version.AssembliesFolderPath = string.Empty;
        }
        if (Directory.Exists(version.ProjectAssembliesFolderPath) == false)
        {
          version.ProjectAssembliesFolderPath = string.Empty;
        }
        if (Directory.Exists(version.BackupFolderPath) == false)
        {
          version.BackupFolderPath = string.Empty;
        }
        if (Directory.Exists(version.VSWebCacheFolderPath) == false)
        {
          version.VSWebCacheFolderPath = string.Empty;
        }
      }
    }
    #endregion

    #region Get By Name Methods
    private string mName = string.Empty;

    public virtual DotNetVersion GetByVSNetName(string name)
    {
      DotNetVersion dc = null;

      mName = name;
      dc = this.Find(FindByName);

      return dc;
    }

    private bool FindByName(DotNetVersion version)
    {
      return (version.Name == mName);
    }
    #endregion
  }
}