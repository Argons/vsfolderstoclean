using System;
using System.IO;
using Microsoft.Win32;

// Taken from http://www.codemag.com/article/0907031
namespace DevFoldersToClean
{
  public class DotNetVersion
  {
    #region Public Properties
    public string ClrPath { get; set; }
    public string TempASPNETPath { get; set; }
    public string BackupFolderPath { get; set; }
    public string AssembliesFolderPath { get; set; }
    public string ProjectAssembliesFolderPath { get; set; }
    public bool ClrExists { get; set; }
    public string Name { get; set; }
    public string VSNETName { get; set; }
    public string VersionNumber { get; set; }
    public string Number { get; set; }
    public string RegKey { get; set; }
    public string BuiltOn { get; set; }
    public string VSWebCacheFolderPath { get; set; }
    #endregion

    #region Init Method
    public void Init()
    {
      bool sixtyFour;

      // Determine if we are running 64 or 32 bit.
      sixtyFour = (IntPtr.Size == 8);
      
      // The following assumes we get back <d>:\Windows\System?? or
      // maybe <d>:\Winnt\System?? or something like that
      // We just want the <d>:\Win... part
      ClrPath = Environment.SystemDirectory;
      ClrPath = ClrPath.Substring(0, ClrPath.LastIndexOf(@"\")+1);
      if(sixtyFour)
        ClrPath += @"Microsoft.NET\Framework64\" + Number;
      else
        ClrPath += @"Microsoft.NET\Framework\" + Number;
      BackupFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + VSNETName + @"\Backup Files";
      TempASPNETPath = ClrPath + @"\Temporary ASP.NET Files";
      VSWebCacheFolderPath = GetVSWebCacheFolderName();
      AssembliesFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\assembly";
      ProjectAssembliesFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Microsoft\VisualStudio\" + VersionNumber + @"\ProjectAssemblies";
      ClrExists = Directory.Exists(ClrPath);
    }
    #endregion

    #region Find VSWebCache folder
    public string GetVSWebCacheFolderName()
    {
      RegistryKey key = null;
      string path = string.Empty;

      try
      {
        //  Find where VSWebCache Folder is located
        key = Registry.CurrentUser.OpenSubKey(RegKey + "WebProject");
        if (key != null)
          path = key.GetValue("OfflineCacheDir").ToString();
      }
      catch
      {
        //  Ignore error
      }
      finally
      {
        if (key != null)
        {
          key.Close();
        }
      }

      if (path == string.Empty)
      {
        path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        if (path.EndsWith("My Documents"))
        {
          path = path.Substring(0, path.IndexOf("My Documents"));
        }
        path = path + @"\VSWebCache";
      }

      if (Directory.Exists(path) == false)
      {
        //  Try this path now: C:\Users\<UserName>\AppData\Local\Microsoft\WebsiteCache
        path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        path += @"\Microsoft\WebsiteCache";
      }

      if (Directory.Exists(path) == false)
      {
        path = "";
      }
      VSWebCacheFolderPath = path;

      return VSWebCacheFolderPath;
    }
    #endregion
  }
}